import React from 'react';
import '../styles/Toolbar.css'
import { NavLink } from 'react-router-dom';
import Socials from './Socials';

function Toolbar({show}) {
  return (
    <>
      <nav className={"flexible center elevate wrap collapses full-width" + (show ? ' show' : '')}>
        <div className="spacer"></div>
        <NavLink to="/">
          <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_256/quickroadsidehelper/iconion.png" alt="iconion" className="iconion"/>
        </NavLink>
        <div className="spacer"></div>
      </nav>
      <nav className={"flexible center elevate wrap"}>
        <Socials />
        <div className="spacer5"></div>
        <NavLink to="/">
          <img src="https://res.cloudinary.com/trinum-daniel/image/upload/w_256/quickroadsidehelper/iconion.png" alt="iconion" className="iconion"/>
        </NavLink>
        <div className="spacer"></div>
        <NavLink to="/services"  activeClassName="active" className="nav-tab hoverable primary bold flexible center uppercase">Services</NavLink>
        <NavLink to="/trail"  activeClassName="active" className="nav-tab hoverable primary bold flexible center uppercase">Our Trail</NavLink>
        <NavLink to="/contact"  activeClassName="active" className="nav-tab hoverable primary bold flexible center uppercase">Contact</NavLink>
        <div className="spacer5"></div>
        <a href="tel:3127749050" className="nav-tab hoverable primary bold flexible center uppercase keep">(312) 774-9050</a>
        <div className="spacer5"></div>
      </nav>
    </>
  )
}

export default Toolbar;
