import React from 'react';
import '../styles/Services.css'
import { IoMdArrowRoundForward, IoMdCheckmark } from 'react-icons/io'
import { IoCall } from 'react-icons/io5'

function Services() {

    const [name, setName] = React.useState('');
    const [loc, setLoc] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [phone, setNumber] = React.useState('');
    const [vin, setVin] = React.useState('');
    const [make, setMake] = React.useState('');
    const [year, setYear] = React.useState('');

    const [service, setService] = React.useState('');
    const [qsSec, setQsSec] = React.useState(1);

    React.useEffect(() => {
      if (service != '') {
        document.getElementById('qs-header-start').scrollIntoView({behavior: 'smooth'});
      }
    }, [service])

    React.useEffect(() => {
      setName('');
      setLoc('');
      setEmail('');
      setVin('');
      setMake('');
    }, [service])

    return (
      <>
        <div id="services" className="svnty-height flexible full-width tertiaryTintFill columns">
          <br/>
          <br/>
          <h4 className="flexible center full-width primary">Our Services</h4>
          <br/>
          <h1 className="flexible center tertiary-dark text-center">We Are Here For You</h1>
          <h3 className="flexible center sm-top tertiary-dark opaque text-center">Select Service to Begin Survey</h3>
          <div className="flexible center rows full-width wrap">
            <div onClick={() => setService('lockouts')} className="flexible columns center srvcs-card clickable hoverable">
              <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1611951471/quickroadsidehelper/locksmith.png" alt="locks"/>
              <h3 className="bold flexible full-width center tertiary-dark">Lock Outs</h3>
              <br/>
              <p className="opaque flexible tertiary-dark center text-center">Left your keys inside your vehicle? Bad days happen! But we are here to help!</p>
            </div>
            <div onClick={() => setService('fuel')} className="flexible columns center srvcs-card clickable hoverable">
              <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1611951471/quickroadsidehelper/fuel.png" alt="fuel"/>
              <h3 className="bold flexible full-width center tertiary-dark">Fuel Delivery</h3>
              <br/>
              <p className="opaque flexible tertiary-dark center text-center">On a rush and forgot to fuel up?! Worry no more! We will take fuel for your vehicle asap!</p>
            </div>
            <div onClick={() => setService('jumpstart')} className="flexible columns center srvcs-card clickable hoverable">
              <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1611951471/quickroadsidehelper/jumpstart.png" alt="jumps"/>
              <h3 className="bold flexible full-width center tertiary-dark">Jump Starts</h3>
              <br/>
              <p className="opaque flexible tertiary-dark center text-center">Battery running low or dead? Where does the red one go? Let us jump start your vehicle!</p>
            </div>
            <div onClick={() => setService('tires')} className="flexible columns center srvcs-card clickable hoverable">
              <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1611951471/quickroadsidehelper/tire.png" alt="tires"/>
              <h3 className="bold flexible full-width center tertiary-dark">Tire Service</h3>
              <br/>
              <p className="opaque flexible tertiary-dark center text-center">Tire went flat? Let us flatten our your problems! We'll go fix your tire super fast!</p>
            </div>
            <div onClick={() => setService('alarm')} className="flexible columns center srvcs-card clickable hoverable">
              <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1611951471/quickroadsidehelper/remote.png" alt="alarm"/>
              <h3 className="bold flexible full-width center tertiary-dark">Alarm Deactivation</h3>
              <br/>
              <p className="opaque flexible tertiary-dark center text-center">Alarm won't stop beeping? We'll go and deactivate it for you in no time!</p>
            </div>
            <div onClick={() => setService('doors')} className="flexible columns center srvcs-card clickable hoverable">
              <img src="https://res.cloudinary.com/trinum-daniel/image/upload/v1611951471/quickroadsidehelper/door.png" alt="doors"/>
              <h3 className="bold flexible full-width center tertiary-dark">Sliding Door Repairs</h3>
              <br/>
              <p className="opaque flexible tertiary-dark center text-center">Sliding doors can be so annoying sometimes! But hey! That's what we are here for!</p>
            </div>
          </div>
          {
            service != "" &&
            <>
              <div className="qs-header-start">
                <div id="qs-header-start"></div>
              </div>
              <h1 id="qs-header" className="flexible center tertiary-dark text-center">Just a few questions...</h1>
              <br/>
              <br/>
              {
                qsSec == 1 &&
                <>
                  <div className="flexible center full-width qs-inp-h columns">
                    <input type="text" name="vin" placeholder="Vehicle Identification Number (VIN)" onChange={(e) => setVin(e.target.value)}/>
                    <input type="text" name="model" placeholder="Vehicle Make & Model" onChange={(e) => setMake(e.target.value)}/>
                    <input type="text" name="year" placeholder="Vehicle Year" onChange={(e) => setYear(e.target.value)}/>
                    <input type="text" name="loc" placeholder="Your Location (500 N Michigan, Chicago IL)" onChange={(e) => setLoc(e.target.value)}/>
                  </div>
                  <br/>
                </>
              }
              {
                qsSec == 2 &&
                <>
                  <div className="flexible center full-width qs-inp-h columns">
                    <input type="name" name="name" placeholder="Full Name" onChange={(e) => setName(e.target.value)}/>
                    <input type="email" name="email" placeholder="E-mail Address" onChange={(e) => setEmail(e.target.value)}/>
                    <input type="tel" name="phone" placeholder="Phone Number" onChange={(e) => setNumber(e.target.value)}/>
                  </div>
                  <br/>
                </>
              }
              {
                qsSec < 2 &&
                <div className="flexible full-width center">
                  <button onClick={() => setQsSec(qsSec + 1)} disabled={vin == '' || make == '' || year == '' || loc == ''} className="flexible center clickable">
                    <IoMdArrowRoundForward className="tertiary"/>
                  </button>
                </div>
              }
              {
                qsSec == 2 &&
                <form action="https://formspree.io/f/mpzonadk" method="POST">
                  <input type="hidden" name="name" value={name}/>
                  <input type="hidden" name="email" value={email}/>
                  <input type="hidden" name="number" value={phone}/>
                  <input type="hidden" name="loc" value={loc}/>
                  <input type="hidden" name="vin" value={vin}/>
                  <input type="hidden" name="make" value={make}/>
                  <input type="hidden" name="year" value={year}/>

                  <div className="flexible full-width center">
                    <button onClick={() => setQsSec(3)} disabled={name == '' || (email == '' && phone == '')} type="submit" className="flexible center clickable">
                      <IoMdCheckmark className="tertiary"/>
                    </button>
                  </div>
                </form>
              }
              {
                qsSec == 3 &&
                <>
                  <h3 className="flexible center tertiary-dark text-center">Thank You!</h3>
                  <h3 className="flexible center tertiary-dark text-center">We'll Get Back to You in 5 Minutes</h3>
                  <br/>
                  <br/>
                  <h3 className="flexible center primary text-center">Urgent? Give Us A Call!</h3>
                  <br/>
                  <div className="flexible full-width center">
                    <button className="flexible center clickable">
                      <a href="tel:3127749050"></a>
                      <IoCall className="tertiary"/>
                    </button>
                  </div>
                </>
              }
            </>
          }
          <br/>
          <br/>
        </div>
      </>
    )
}

export default Services;
