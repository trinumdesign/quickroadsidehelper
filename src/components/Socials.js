import React from 'react';
import '../styles/Socials.css';

import { FaFacebookF, FaInstagram } from 'react-icons/fa'
import { AiFillMessage } from 'react-icons/ai'
import { FiMenu } from 'react-icons/fi'
import { HiLocationMarker } from 'react-icons/hi'
import { IoCall, IoMail } from 'react-icons/io5'
import { NavLink } from 'react-router-dom';

function Socials({}) {
  return (
    <>
      <div className="socials-side resize-hide flexible row center">
        <div className="flexible clickable center rows content">
          <div className="ico-cont flexible center">
            <a target="_blank" href="mailto:contact@quickroadsidehelper.com"></a>
            <IoMail className="tertiary" />
          </div>
          <div className="ico-cont flexible center">
            <a target="_blank" href="tel:3127749050"></a>
            <IoCall className="tertiary" />
          </div>
          <div className="flexible center tertiary">
            Quick Roadside Helper
          </div>
          <div className="ico-cont flexible center">
            <a target="_blank" href="https://www.facebook.com/QuickRoadsideHelper"></a>
            <FaFacebookF className="tertiary" />
          </div>
        </div>
      </div>
      <div className="fabs-bar flexible row center resize-show">
        <div className={"fab flexible center popup primaryFill show"}>
          <a target="_blank" href="mailto:contact@quickroadsidehelper.com"></a>
          <IoMail className="tertiary" />
        </div>
        <div className={"fab flexible center popup primaryFill show"}>
          <a target="_blank" href="tel:3127749050"></a>
          <IoCall className="tertiary" />
        </div>
        <div className={"fab flexible center popup primaryFill show"}>
          <a href="https://www.facebook.com/QuickRoadsideHelper"></a>
          <FaFacebookF className="tertiary" />
        </div>
      </div>
    </>
  )
}

export default Socials;
