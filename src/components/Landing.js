import { NavLink } from 'react-router-dom';

import '../styles/Landing.css'

function Landing() {
  return (
    <>
      <div id="landing" className="svnty-height has-bg flexible full-width columns">
        <div className="shadow"></div>
        <div className="spacer20"></div>
        <h1 className="viga bold tertiary landing-header anim flexible center text-center spacer">Because Things Happen,<br/>We Are Here to Help</h1>
        <h2 className="viga bold landing-desc tertiary text-center spacer">Call Now: <a href="tel:3127749050" className="hoverable phn secondary">(312) 774-9050</a><br/>Or: <NavLink to="/services" className="hoverable phn secondary">Take Survey Below</NavLink></h2>
      </div>
    </>
  )
}

export default Landing;
