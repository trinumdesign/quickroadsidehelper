import React from 'react';
import '../styles/Trail.css'
import { AiFillMessage } from 'react-icons/ai'
const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

mapboxgl.accessToken = 'pk.eyJ1IjoiZGFubnktdHJpbnVtIiwiYSI6ImNra2czZncwYzBzdm0yb3F0MnVraHgxenQifQ._B8WXck5l1kOGubes69igQ';

function Trail() {
  const markers = [
    [[-87.754768, 41.919579], 'tire'],
    [[-87.699173, 41.920609]],
    [[-87.739822, 41.899078]],
    [[-87.636162, 41.918610]],
    [[-87.749161, 41.851158]],
    [[-87.856610, 41.893430]],
    [[-87.839149, 41.886250]],
    [[-87.801003, 42.025768]],
    [[-87.651970, 41.926560], 'jumpstart'],
    [[-87.753390, 41.960720], 'locksmith'],
    [[-87.636430, 41.919520], 'jumpstart'],
    [[-87.669430, 41.972410], 'tire'],
    [[-87.718570, 41.915620], 'tire']
  ];
  let map;

  const setMap = () => {
    map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-87.754768, 41.91957],
      zoom: 10
    });
    map.addControl(new mapboxgl.NavigationControl());
    map.on('load', () => {
      for (let coordinate of markers) {
        let elem = document.createElement('div');
        elem.style.width = '30px';
        elem.style.height = '30px';
        elem.className = 'map-marker popup';
        if (coordinate.length > 1) {
          elem.style.backgroundImage = `url(https://res.cloudinary.com/trinum-daniel/image/upload/bo_40px_solid_rgb:fff,e_replace_color:ff3600:50,w_254/quickroadsidehelper/${coordinate[1]}.png)`;
        } else {
          elem.style.backgroundImage = `url(https://res.cloudinary.com/trinum-daniel/image/upload/bo_40px_solid_rgb:fff,w_254/quickroadsidehelper/iconion.png)`;
        }
        new mapboxgl.Marker(elem).setLngLat(coordinate[0]).addTo(map);
      }
      map.addSource("source_circle_500", {
        "type": "geojson",
        "data": {
          "type": "FeatureCollection",
          "features": [{
            "type": "Feature",
            "geometry": {
              "type": "Point",
              "coordinates": [-87.754768, 41.91957]
            }
          }]
        }
      });
      map.addLayer({
        "id": "circle500",
        "type": "circle",
        "source": "source_circle_500",
        "paint": {
          "circle-radius": {
            stops: [
              [0, 6],
              [12, 500],
              [15, 3000],
            ],
            base: 1.75
          },
          "circle-color": "#172290",
          "circle-opacity": 0.2
        }
      });
      map.scrollZoom.disable()
    });
  }

  React.useEffect(() => {
    setMap();
  }, [])

    return (
      <>
        <div id="trail" className="svnty-height flexible full-width columns">
          <br/>
          <br/>
          <h4 className="flexible center full-width primary">Our Trail</h4>
          <br/>
          <h1 className="flexible center tertiary-dark text-center">Here's Who We've Helped</h1>
          <br/>
          <div id="map"></div>
        </div>
      </>
    )
}

export default Trail;
