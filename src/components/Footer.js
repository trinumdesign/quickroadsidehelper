import React from 'react';
import Socials from './Socials';
import { FaFacebookF, FaInstagram } from 'react-icons/fa'
import { AiFillMessage } from 'react-icons/ai'
import { FiMenu } from 'react-icons/fi'
import { HiLocationMarker } from 'react-icons/hi'
import { IoCall, IoMail } from 'react-icons/io5'

import '../styles/Footer.css'

function Footer() {
  return (
    <>
      <section id="footer" className="footer full-width flexible center columns">
        <div className="flexible full-width center footer-cnt">
          <div className="ico-cont flexible center hoverable">
            <a target="_blank" href="mailto:contact@quickroadsidehelper.com"></a>
            <IoMail className="primary" />
          </div>
          <div className="ico-cont flexible center hoverable">
            <a target="_blank" href="tel:3127749050"></a>
            <IoCall className="primary" />
          </div>
          <div className="ico-cont flexible center hoverable">
            <a target="_blank" href="https://www.facebook.com/QuickRoadsideHelper"></a>
            <FaFacebookF className="primary" />
          </div>
        </div>
        <h3 className="flexible center full-width primary">We Are Here For You 24/7</h3>
        <h5 className="copyright-creds full-width center flexible">© 2021 Quick Roadside Helper. All Rights Reserved. Developed by<a className="creds-ref" href="https://www.trinumdesign.com">&nbsp;Trinum Design Inc.</a></h5>
      </section>
    </>
  )
}

export default Footer;
