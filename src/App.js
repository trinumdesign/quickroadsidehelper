import React from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import { IoCall } from 'react-icons/io5'

import Toolbar from './components/Toolbar';
import Landing from './components/Landing';
import Services from './components/Services';
import Trail from './components/Trail';
import Footer from './components/Footer';

class App extends React.Component {
  state = {
    showFab: false
  }

  constructor(props) {
    super(props);

    this.state = {
      showFab: false
    }

    setTimeout(() => {
      this.onRouteChange(props.location, '');
    }, 800);

    props.history.listen((data) => {
      setTimeout(() => {
        this.onRouteChange(data, props.history);
      }, 500);
    });
  }

  onRouteChange(data, history) {
    if (history.action === "REPLACE") return;
    let element;

    switch(data.pathname) {
      case '/':
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
        this.setState({
          showFab: false
        });
        break;
      case '/services':
        element = document.getElementById('services');
        this.setState({
          showFab: false
        });
        break;
      case '/trail':
        element = document.getElementById('trail');
        this.setState({
          showFab: false
        });
        break;
      case '/contact':
        element = document.getElementById('footer');
        this.setState({
          showFab: false
        });
        break;
      default:
        this.setState({
          showFab: false,
          location: data
        });
        break;
    }

    if (!element) return;
    element.scrollIntoView({behavior: 'smooth'});
  }

  componentDidMount() {
    document.addEventListener('scroll', (e) => {
      this.setState({
        showFab: window.scrollY > 86
      })
    });
  }

  render() {
    return (
      <React.Fragment>
        <Toolbar show={this.state.showFab} />
        <Landing />
        <Services />
        <Trail />
        <Footer />
        <div className={"fab flexible center popup primaryFill resize-hide" + (this.state.showFab ? ' show' : '')}>
          <a href="tel:3127749050"></a>
          <IoCall className="tertiary" />
        </div>
      </React.Fragment>
    )
  }
}

export default withRouter(App);
